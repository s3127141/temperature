package nl.utwente.di.bookQuote;  import java.util.HashMap;  public class Quoter {
    HashMap<String,Double> library = new HashMap<>();
    int temperature;
    public Quoter(){
        library.put("1",10.0);
        library.put("2",45.0);
        library.put("3",20.0);
        library.put("4",35.0);
        library.put("5",50.0);
    }
    double getTemperature(int temperature){
        return ((double) (temperature * 9) /5) + 32 ;
    }
}